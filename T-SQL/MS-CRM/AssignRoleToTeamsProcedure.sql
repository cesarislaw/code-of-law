-- =============================================
-- Created by: Cesar Giralt
-- Created date: 1/4/2013
-- Purpose:	Adds a record to TeamRoles foreign key table with the keys for teams matching a naming pattern and the key for the security role provided.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'usp_AssignRoleToTeams' AND type = 'P')
BEGIN
  DROP PROCEDURE usp_AssignRoleToTeams
END
GO

CREATE PROCEDURE usp_AssignRoleToTeams
	@RoleName				VARCHAR(30)	= '',
	@TeamNamePattern		VARCHAR(30)	= '',
	@BusinessUnitTeamsOnly	BIT			= 0
AS
BEGIN
INSERT INTO [TeamRoles]
	(TeamRoles.TeamId, TeamRoles.RoleId)
SELECT
	TeamsNotAssignedToRole.TeamId, SecurityRoles.RoleId
FROM
	(
		SELECT [TeamId],[BusinessUnitId] FROM [TeamBase] Teams
		WHERE NOT EXISTS
		(
			SELECT NULL
			FROM [TeamRoles] TeamRoles
			JOIN [RoleBase] Roles ON TeamRoles.RoleId = Roles.RoleId
			WHERE
				Teams.TeamId = TeamRoles.TeamId
				AND TeamRoles.RoleId = Roles.RoleId
		)
		AND Name LIKE @TeamNamePattern
		AND (IsDefault = 1 OR @BusinessUnitTeamsOnly <> 0)
	) TeamsNotAssignedToRole
JOIN [RoleBase] SecurityRoles 
	ON SecurityRoles.BusinessUnitId = TeamsNotAssignedToRole.BusinessUnitId
WHERE 
	SecurityRoles.Name Like @RoleName
END
GO