-- =============================================
-- Created by: Cesar Giralt
-- Created date: 1/4/2013
-- Purpose:	Adds a record in the SystemUserRoles foreign key table with the key for a user with the first security role and the key for the second security role.
--			In short it grabs all users with one security role and assigns them a new one.
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'usp_AssignRoleToUsersWithRole' AND type = 'P')
BEGIN
  DROP PROCEDURE usp_AssignRoleToUsersWithRole
END
GO

CREATE PROCEDURE usp_AssignRoleToUsersWithRole
	@NewRole		VARCHAR(30)	= '',
	@CurrentRole	VARCHAR(30)	= ''
AS
BEGIN
INSERT INTO SystemUserRoles
	(SystemUserRoles.SystemUserId, SystemUserRoles.RoleId)
SELECT
	UsersWithRole.SystemUserId, SecurityRoles.RoleId
FROM
	(
		SELECT USERS.SystemUserId, Roles.BusinessUnitId
		FROM SystemUserBase Users
		JOIN SystemUserRoles UserRoles ON USERS.SystemUserId = UserRoles.SystemUserId
		JOIN RoleBase ROLES ON UserRoles.RoleId = Roles.RoleId
		WHERE Roles.Name LIKE @CurrentRole
	) UsersWithRole
JOIN [RoleBase] SecurityRoles
	ON SecurityRoles.BusinessUnitId = UsersWithRole.BusinessUnitId
WHERE
	SecurityRoles.Name LIKE @NewRole
	AND NOT EXISTS
	(
		SELECT NULL
		FROM SystemUserRoles UserRoles
		JOIN [RoleBase] Roles ON UserRoles.RoleId = Roles.RoleId
		WHERE Roles.Name LIKE @NewRole AND UserRoles.SystemUserId = UsersWithRole.SystemUserId
	)
END
GO